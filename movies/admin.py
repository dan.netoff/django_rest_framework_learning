from django.contrib import admin
from django import forms

from .models import Category, Movie, Actor, RatingStar, Genre, Rating, Reviews


class MovieAdminForm(forms.ModelForm):
    description = forms.CharField(label="Описание")

    class Meta:
        model = Movie
        fields = '__all__'


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "url")
    list_display_links = ("name",)


@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    list_display = ("name", "url")
    list_display_links = ("name",)


@admin.register(Reviews)
class GenreAdmin(admin.ModelAdmin):
    list_display = ("name", "email", "parent", "movie", "id")
    readonly_fields = ("name", "email")


class ReviewInLine(admin.TabularInline):
    model = Reviews
    extra = 1
    readonly_fields = ("name", "email")


@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    list_display = ["title", "category", "url", "draft"]
    list_filter = ("category", "year")
    save_on_top = True
    save_as = True
    list_editable = ("draft",)


@admin.register(Actor)
class GenreAdmin(admin.ModelAdmin):
    model = Actor